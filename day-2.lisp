(defpackage :day-2
  (:use :common-lisp)
  (:export :display-results))

(in-package :day-2)

(defparameter *input-file* #P"/home/Sora/Programming/advent-of-code-2021/inputs/day-2.txt")

(defun parse-data (file)
  (with-open-file (stream file)
    (loop for line = (read-line stream nil)
	  while line
	  collect (destructuring-bind (dir count) (cl-utilities:split-sequence #\Space line)
		    (list (intern (string-upcase dir) :keyword) (parse-integer count))))))

(defparameter *input-data* (parse-data *input-file*))

(defun do-block-motion (current-pos command)
  (destructuring-bind (y x) current-pos
    (destructuring-bind (dir count) command
      (case dir
	(:up (list (- y count) x))
	(:down (list (+ y count) x))
	(:forward (list y (+ x count)))))))

(defun part-1 (lst)
  (apply #'* (reduce #'do-block-motion lst
		     :initial-value '(0 0))))

(defun do-aim-motion (current-pos command)
  (destructuring-bind (y x aim) current-pos
    (destructuring-bind (dir count) command
      (case dir
	(:up (list y x (- aim count)))
	(:down (list y x (+ aim count)))
	(:forward (list (+ (* count aim) y) (+ count x) aim))))))

(defun part-2 (lst)
  (apply #'* (subseq (reduce #'do-aim-motion lst
			     :initial-value '(0 0 0))
		     0
		     2)))

(defun display-results ()
  (format t "~&Part 1: ~a~%" (part-1 *input-data*))
  (format t "~&Part 2: ~a~%" (part-2 *input-data*)))
