(defpackage :day-7
  (:use :common-lisp)
  (:export :display-results))

(in-package :day-7)

(defparameter *input-file* #P"/home/Sora/Programming/advent-of-code-2021/inputs/day-7.txt")

(defun parse-data (file)
  (with-open-file (stream file)
    (apply #'append
	   (loop for line = (read-line stream nil)
		 while line
		 collect (mapcar #'parse-integer
				 (cl-utilities:split-sequence #\, line))))))

(defparameter *input-data* (parse-data *input-file*))

(defun part-1 (lst)
  (let ((start-points (remove-duplicates lst)))
    (loop for point in start-points
	  minimizing (reduce #'+ (mapcar (lambda (n)
					   (abs (- point n)))
					 lst)))))

(defun sum-to (x)
  (/ (* x (1+ x)) 2))

(defun part-2 (lst)
  (let ((start-points (loop for i from 1 to (length lst) collecting i)))
    (loop for point in start-points
	  minimizing (reduce #'+ (mapcar (lambda (n)
					   (sum-to (abs (- point n))))
					 lst)))))

(defun display-results ()
  (format t "~&Part 1: ~a~%" (part-1 *input-data*))
  (format t "~&Part 2: ~a~%" (part-2 *input-data*)))
