(defpackage :day-1-asd
	    (:use :common-lisp))

(in-package :day-1-asd)

(asdf:defsystem :day-1
  :components ((:file "day-1")))

(uiop:define-package :day-1
  (:use :common-lisp))
