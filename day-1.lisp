(defpackage :day-1
  (:use :common-lisp)
  (:export :display-results))

(in-package :day-1)

(defparameter *input-file* #P"/home/Sora/Programming/advent-of-code-2021/inputs/day-1.txt")

(defun parse-data (file)
  (with-open-file (stream file)
    (loop for line = (read-line stream nil)
	  while line collect (parse-integer line))))

(defparameter *input-data* (parse-data *input-file*))

(defun part-1 (lst)
  (loop for (a b) on lst by #'cdr
	count (and b (< a b))))

(defun part-2 (lst)
  (part-1 (loop for (x y z) on lst by #'cdr
		when z collect (+ x y z))))

;; Technically it's possible to use only one loop instead of nesting them, but I
;; think the nested loop version is cleaner and more clearly expresses the
;; intent.
(defun part-2-alt (lst)
  (loop for (a b c x y z) on lst by #'cdr
	count (and z (< (+ a b c) (+ x y z)))))

(defun display-results ()
  (format t "~&Part 1: ~a~%" (part-1 *input-data*))
  (format t "~&Part 2: ~a~%" (part-2 *input-data*)))
