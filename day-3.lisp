(defpackage :day-3
  (:use :common-lisp)
  (:export :display-results))

(in-package :day-3)

(defparameter *input-file* #P"/home/Sora/Programming/advent-of-code-2021/inputs/day-3.txt")

(defun parse-data (file)
  (with-open-file (stream file)
    (loop for line = (read-line stream nil)
	  while line collect (coerce line 'list))))

(defparameter *input-data* (parse-data *input-file*))

(defun bit-counts (numbers)
  (apply #'mapcar (lambda (&rest bits) (count #\1 bits)) numbers))

(defun bit-means (bit-counts threshold)
  (mapcar (lambda (c)
	    (if (> c threshold)
		#\1
		#\0))
	  bit-counts))

(defun bit-list-not (bit-list)
  (mapcar (lambda (b)
	    (if (char= b #\1)
		#\0
		#\1))
	  bit-list))

(defun bit-list->int (bit-list)
  (parse-integer (coerce bit-list 'string) :radix 2))

(defun part-1 (lst)
  (let* ((gamma-bits (bit-means (bit-counts lst) (/ (length lst) 2)))
	 (epsilon-bits (bit-list-not gamma-bits))
	 (gamma (bit-list->int gamma-bits))
	 (epsilon (bit-list->int epsilon-bits)))
    (* gamma epsilon)))

(defun bit-searcher (candidates pos compare-fun tiebreaker)
  (if (= (length candidates) 1)
      candidates
      (let* ((threshold (/ (length candidates) 2))
	     (pos-count (count #\1 (mapcar (lambda (l) (nth pos l))
					   candidates)))
	     (keep-bit (cond ((= threshold pos-count) tiebreaker)
			     ((funcall compare-fun pos-count threshold) #\1)
			     (t #\0))))
	(remove-if-not (lambda (bit-list)
			 (char= keep-bit (nth pos bit-list)))
		       candidates))))

(defun number-searcher (candidates compare-fun tiebreaker)
  (reduce (lambda (c p)
	    (bit-searcher c p compare-fun tiebreaker))
	  (loop for i below (length (first candidates)) collect i)
	  :initial-value candidates))

(defun part-2 (lst)
  (let ((oxy-gen-rating (bit-list->int (first (number-searcher lst #'> #\1))))
	(co2-scrub-rating (bit-list->int (first (number-searcher lst #'< #\0)))))
    (* oxy-gen-rating co2-scrub-rating)))

(defun display-results ()
  (format t "~&Part 1: ~a~%" (part-1 *input-data*))
  (format t "~&Part 2: ~a~%" (part-2 *input-data*)))
