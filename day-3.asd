(defpackage :day-3-asd
	    (:use :common-lisp))

(in-package :day-3-asd)

(asdf:defsystem :day-3
  :components ((:file "day-3")))

(uiop:define-package :day-3
  (:use :common-lisp))
