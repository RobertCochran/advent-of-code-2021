(defpackage :day-7-asd
	    (:use :common-lisp))

(in-package :day-7-asd)

(asdf:defsystem :day-7
  :components ((:file "day-7"))
  :depends-on ("cl-utilities"))

(uiop:define-package :day-7
  (:use :common-lisp))
