(defpackage :day-2-asd
	    (:use :common-lisp))

(in-package :day-2-asd)

(asdf:defsystem :day-2
  :components ((:file "day-2"))
  :depends-on ("cl-utilities"))

(uiop:define-package :day-2
  (:use :common-lisp))
